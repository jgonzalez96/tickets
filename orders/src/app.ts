import 'express-async-errors';

import { NotFoundError, currentUser, errorHandler } from '@jgttickets/common';

import cookieSession from 'cookie-session';
import { createOrderRouter } from './routes/new';
import { deleteOrderRouter } from './routes/delete';
import express from 'express';
import { indexOrderRouter } from './routes';
import { showOrderRouter } from './routes/show';

const app = express();
app.set('trust proxy', true);
app.use(express.json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test'
  })
);
app.use(currentUser);

app.use(createOrderRouter);
app.use(showOrderRouter);
app.use(indexOrderRouter);
app.use(deleteOrderRouter);

app.all('*', async () => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
