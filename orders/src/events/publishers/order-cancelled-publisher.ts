import { Publisher, OrderCancelledEvent, Subjects } from '@jgttickets/common';

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
  readonly subject = Subjects.OrderCancelled;
}
