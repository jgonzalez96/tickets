import {
  ExpirationCompleteEvent,
  Publisher,
  Subjects
} from '@jgttickets/common';

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
  readonly subject = Subjects.ExpirationComplete;
}
