import { Publisher, Subjects, TicketCreatedEvent } from '@jgttickets/common';

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
  readonly subject = Subjects.TicketCreated;
}
