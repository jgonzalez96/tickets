import { Publisher, Subjects, TicketUpdatedEvent } from '@jgttickets/common';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  readonly subject = Subjects.TicketUpdated;
}
